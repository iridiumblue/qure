
// this is the background code...

// listen for our browerAction to be clicked

//var firstTimeOnly = true;

var bannerAlreadyShown=false;

chrome.tabs.onUpdated.addListener(function (tab , info) {
  if (info.status === 'complete') {   
     chrome.tabs.executeScript(tab.id, {
     	   file: 'inject.js'
	 });
  }
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.name == "banner_already_shown") {
      sendResponse({status: bannerAlreadyShown});
      bannerAlreadyShown=true;
    }
});


chrome.browserAction.onClicked.addListener(function(tab) {
    chrome.tabs.create({'url': chrome.extension.getURL('/options.html'), 'selected': true});
});
