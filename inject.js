
function skippast(txt,token)
{
  var pos = txt.indexOf(token);
  //console.log('~~~ skippast ' + token + ' ' + pos);
  if (pos<0) {
     console.log("skippast : error locating " + token);
     return '';
  }
  pos += token.length;
  return txt.substr(pos);
}

function scanto(txt,token,lastone = false) 
{
  var pos = 0;
  if (!lastone) {
    pos = txt.indexOf(token)
  } else {
    pos = txt.lastIndexOf(token);
  }
  if (pos<0) {
     console.log("scanto : error locating " + token);
     return '';
  }
  return txt.substr(0,pos);


}
function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

function showBanner()
{
    var div = document.createElement('div');
    div.id = 'qure_id';
    div.style.width = '100%';
    div.style.height='15px';
    div.style.backgroundColor = '#444444';
    div.style.color = 'white';
    div.style.paddingTop = '5px';
    div.style.paddingBottom = '5px';
    div.style.textAlign='center';
    div.innerHTML = 'The Qure extension is modifying your feed.  Click the syringe icon to the right of your address bar to tweak settings.</a>';
    var header = document.getElementsByClassName('SiteHeader')[0];
    header.appendChild(div);
    setTimeout(function(){ fade(div); }, 5000);
}

function getAsker(qhref)
{
   //console.log(qhref + "/log");
   var myFetch = fetch(qhref + "/log");

   myFetch.then(function(response) {
        response.text().then(function(text) {
          try {
            var url = response.url;
            
            url = scanto(url,'/log');
            ancestor = window.qlinks_seen[url];
            //console.log(text);
           
            text=skippast(text,'Question Log');
             
            text = skippast(text,'Question added by');
            if (text=='') {
               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by ??</span>';
            } else if (text.indexOf('Anonymous')>-1) {
               if (window.anon == 'highlight') {
                    ancestor.style.backgroundColor=window.anon_color;
               } else {
                    ancestor.style.height=0;
                    ancestor.style.display="none";
               }

               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by Anonymous</span>';
               
            } else {
               text = skippast(text,"<a class='user' href='");
               text = scanto(text,"'");
               
               uname = skippast(text,"/profile/").replace(/-/g,' ');
               console.log(uname);
               asker = '</a><span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by <a href="/profile/' + text + '">' + uname + '</a></span>';
            }
               
            qtext = ancestor.querySelector('.ui_qtext_rendered_qtext');
            qtext.outerHTML = qtext.outerHTML + asker;
            
          } catch (err) {
             console.log(err);
             //alert(err);
          }
      });
  });
}

function scanForAnonQ()
{

    var questions = document.getElementsByClassName("question_link");
    var i=0;
    for (i=0; i<questions.length; i++) {
       var ancestor = questions[i].closest('.multifeed_bundle_story');
       if (ancestor == null) continue;
       //alert(ancestor.innerHTML);

       var href = questions[i].href;
       
       if (!(href in window.qlinks_seen)) {
          //console.log(href);
          window.qlinks_seen[href] = ancestor;
          getAsker(href);
       }
    }
  
}





(function() {

   window.qlinks_seen = {};
   if ('already_fired' in window) {
     return;
   }
   window.already_fired = true;
   
   var url = window.location.href;
   only_url = "quora.com/";
   if ((url.indexOf('profile')<0) && (url.slice(0-only_url.length) != only_url)) {
     
     //return;
   }




   // Watch for scroll events.
   
   

   var timer = null;


   newStyle = '';
   chrome.storage.sync.get({
       "mode": "highlight", 
       "anon": "highlight",
       "anon_color" : "#ffffcc",
       "link_color" : "#e7f4e6"
     }, function(items) {
       var mode = items["mode"];
       var anon = items["anon"];
       window.anon_color = items["anon_color"];
       var link_color = items["link_color"];
       //chrome.tabs.getCurrent(function(tab){ console.log(JSON.stringify(tab,null, 2)); });
        try {
          chrome.runtime.sendMessage({name: "banner_already_shown"}, function(response) {
            console.log(response.status);
            if ( !response.status && (mode != 'chill')) {
               showBanner();
            }
          });
        } catch(err) {
          ;
        }
       if (mode == 'chill') {
         newStyle='';
       } else if (mode == 'suppressed') {
         
         newStyle = '.HyperLinkFeedStory {display: none !important; height:0px !important;}';
       } else {
         
         newStyle = '.HyperLinkFeedStory {background-color: ' + link_color + ' !important;}';
       }
   
       // ui_qtext_more_link

       // Somebody ... brought a knife to a gun fight.
       newStyle += '.ui_layout_content_wrapper {all: initial; * {all: unset;}}';
       newStyle += '.u-relative {all: initial; * {all: unset;}}';
       newStyle += '.u-width--100 {all: initial; * {all: unset;}} ';
       newStyle += '.u-margin-right--sm {all: initial; * {all: unset;}} ';
       newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset;}} ';
       newStyle += '.ui_qtext_truncate_4 {all: initial; * {all: unset;}} ';
       newStyle += '.u-bg--cover {all: initial; * {all: unset;}}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {all: initial; * {all: unset;}}';
       //newStyle += '.ui_layout_thumbnail {all: initial; * {all: unset;}'};


       newStyle += '.ui_layout_content_wrapper {width:100% !important;}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {display:none !important;}';

       //newStyle += '.ui_qtext_rendered_qtext {width:572px !important}';

       //newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset;}} ';
       //newStyle += '.ui_qtext_rendered_qtext {font-size:15px; font-weight:400;}';    
       newStyle += '.ui_story_title ui_story_title_medium.ui_qtext_rendered_qtext {font-size: 18px; font-weight:700 !important}'; 

       newStyle += '.ui_qtext_truncated_text {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_qtext_truncated {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_header_image_wrapper {display:none !important};';
       newStyle += '.u-bg--cover {height:0px !important; width:0px !important; display:none !important};';
       newStyle += '.u-width--100 {width:100% !important};';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {width: 0px !important; display:none !important;}';
       newStyle += '.feed_card_on .Bundle .full_bundle {border: 1px solid #efefef !important; border-bottom: 0px;}';
       newStyle += '.sticky_bundle_wrapper {display: none;}';
       newStyle = '<head><style>' + newStyle + '</style></head>';

       document.body.insertAdjacentHTML( 'afterbegin', newStyle);

       if (anon != 'show') {
           window.anon = anon;
           scanForAnonQ();        
           //https://stackoverflow.com/questions/4620906/how-do-i-know-when-ive-stopped-scrolling-javascript
           window.addEventListener('scroll', function() {
                if(timer !== null) {
                    clearTimeout(timer);        
                }
                timer = setTimeout(function() {
                      scanForAnonQ();
                }, 150);
           }, false);
        }     
      
    });

})();