


// Saves options to chrome.storage
function save_options() {

   var val = "";
   var val2 = "";

   document.getElementById('link_color').backgroundColor;
   document.getElementById('anon_color').backgroundColor;

   if (document.getElementById('radio1').checked) {
      val = "highlight";
   } else if (document.getElementById('radio2').checked) {
      val = "suppressed";
   } else {
      val = "chill";
   }

   if (document.getElementById('radio21').checked) {
      val2 = "highlight";
   } else if (document.getElementById('radio22').checked) {
      val2 = "hide";
   } else {
      val2 = "show";
   }

   chrome.storage.sync.set(
     {
       'mode': val, 
       'anon':val2,
       'link_color' : document.getElementById('link_color').value,
       'anon_color' : document.getElementById('anon_color').value
     }, function(){
     setTimeout(function() {
      window.close();
    }, 250);
   });
 
}


// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
 
  chrome.storage.sync.get({
    "mode": "highlight",
    "anon": "highlight",
    "link_color": "#ffffcc",
    "anon_color":"#e7f4e6"  
  }, function(items) {
     var mode = items["mode"];
     var anon = items["anon"];
     document.getElementById('link_color').value=items["link_color"];
     document.getElementById('anon_color').value=items["anon_color"];

     if (mode == 'highlight') {
        document.getElementById('radio1').checked = true;
     } else if (mode == 'suppressed') {
        document.getElementById('radio2').checked = true;
     } else {
        document.getElementById('radio3').checked = true;
     }
     if (anon == 'highlight') {
        document.getElementById('radio21').checked = true;
     } else if (anon == 'hide') {
        document.getElementById('radio22').checked = true;
     } else {
        document.getElementById('radio23').checked = true;
     }
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',save_options);
aPicker = document.getElementById('anon_color');


function watchColorPicker(event) {
    console.log("----");
    //document.getElementById('anon_samp').style.backgroundColor = event.target.value;
}

aPicker.addEventListener("change", watchColorPicker, false);