
g_asker_pool = {};

/*
    time_stamp;
    # questions
    is_pumper
*/

//g_muted_pumpers = {};
/*
*/

function skippast(txt,token)
{
  var pos = txt.indexOf(token);
  //console.log('~~~ skippast ' + token + ' ' + pos);
  if (pos<0) {
     console.log("skippast : error locating " + token);
     return '';
  }
  pos += token.length;
  return txt.substr(pos);
}

function scanto(txt,token,lastone = false) 
{
  var pos = 0;
  if (!lastone) {
    pos = txt.indexOf(token)
  } else {
    pos = txt.lastIndexOf(token);
  }
  if (pos<0) {
     console.log("scanto : error locating " + token);
     return '';
  }
  return txt.substr(0,pos);


}

function fade(element) {
    var op = 1;  // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}

function showBanner()
{
    var div = document.createElement('div');
    div.id = 'qure_id';
    div.style.width = '100%';
    div.style.height='15px';
    div.style.backgroundColor = '#444444';
    div.style.color = 'white';
    div.style.paddingTop = '5px';
    div.style.paddingBottom = '5px';
    div.style.textAlign='center';
    div.innerHTML = 'The Qure extension is modifying your feed.  Click the syringe icon to the right of your address bar to tweak settings.</a>';
    var header = document.getElementsByClassName('SiteHeader')[0];
    header.appendChild(div);
    setTimeout(function(){ fade(div); }, 5000);
}

function wait_for_it(el)
{
   el = skippast(el,"PagedListMoreButton");
   console.log(el); 
}

// A handful of heuristics to define a question pumper without having to query the server for too much crap.
// Pump iff :
// # Questions > 10
// Questions > 20% of Answers

function pumpTest(uname)
{
	if (!g_asker_pool.hasOwnProperty(uname)) {return false;};
	asker = g_asker_pool[uname];
	if (asker.questions>10) {
		return true;
    }
}
/*
function qure_collapse(element) {
    var height = element.
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 50);
}
*/

function recursiveColor(nd,color)
{
  if (nd.style) {
    nd.style.cssText="background-color:#999999 !important;" + nd.style.cssText;
    nd.style.setProperty("backgroundColor", "inline", "!important");
    //nd.style.backgroundColor=color + ' !important';
  }
  var children = nd.childNodes;
  for(var i=0; i < children.length; i++) {
    recursiveColor(children[i]);
  }
}

function save_persistent()
{
  chrome.storage.local.set({
      'g_asker_pool': g_asker_pool
    });
  
}

function load_persistent()
{
    chrome.storage.local.get({
      "g_asker_pool": {}
     
    }, function(items) {
       g_asker_pool = items["g_asker_pool"];
       g_year = items["g_year"];
       g_items = items["g_items"];
       g_answerIdx = items["g_answerIdx"];
       NUM_DOCS = items["NUM_DOCS"];
       g_items_set = items["g_items_set"];
       g_first_item = items["g_first_item"];
       g_save_text = items["g_save_text"];
       g_misc = items["g_misc"];
       g_timestamp = items["g_timestamp"];

       after_load();
    });
}



function qure_click(obj, uname)
{
  recursiveColor(obj,"#AAAAAA");
  setTimeout(function(x){
     recursiveColor(x,"#AAAAAA");
  }(obj), 50);
  
  obj.style.maxHeight="0px";
  console.log("Adding " + uname + " to shitlist.");
  g_asker_pool[uname].is_muted=true;
}


function qure_mute_hover(obj,highlight)
{

	obj.style.backgroundColor= highlight ? "white" : "#CCCCCC";
	obj.style.textDecoration= highlight ? "underline" : "none";

}

function displayPumpQuestion(ancestor,uname,hname)
{
	
    ancestor.style.backgroundColor="#CCCCCC";
    ancestor.className += ancestor.className + " qure_mute_class";
     muteOption ='<div  style="float:top; margin-bottom:0px;padding-top:3px; margin-left:3px; height:26px;"><span class="qure_mute_option" style="cursor:pointer; border-radius:5px; position:relative; left:-1px; top:-2px;"><img style="position:relative; top:4px;"  src="' + chrome.extension.getURL("images/mute.png") +  '" width="20" height="20">Mute questions from </span> <a style="position:relative; left:-1px; top:-2px;" href="' + uname+'">' + hname + '</a> Questions:' + g_asker_pool[uname].questions + '  Answers:' + g_asker_pool[uname].answers +' </div>'

    ancestor.insertAdjacentHTML( 'afterbegin', muteOption);  
    muteDom = ancestor.getElementsByClassName("qure_mute_option")[0];
    muteDom.addEventListener("mouseover", 
        // Hold my beer whilst I bust out a closure.
        (function(obj){return function(){qure_mute_hover(obj, true);};})(muteDom),      		
         false);
        // told you I would.

    muteDom.addEventListener("mouseout", 
        (function(obj){return function(){qure_mute_hover(obj, false);};})(muteDom), 
         false);

    muteDom.addEventListener("click",      		
        (function(obj,user){return function(){qure_click(obj,user);};})(ancestor,uname),           		
         false);
        
}

function checkForQuestionPump(uname, ancestor, hname)
{
	//muteOption = '<div style="float:top; margin-bottom:-18px;"><img width=20 height=20 src="' + chrome.extension.getURL("images/mute.png") + '"/></div>';
    if (uname in g_asker_pool) {
    	console.log("Recognize author :" + hname);
    	who = g_asker_pool[uname];
    	if (who.is_muted) {
    		console.log("Not showing " + hname);
    		ancestor.style.display="none";
    	} else if (who.is_pumper) {
    		console.log("Shortcut pump display " + hname);
    		displayPumpQuestion(ancestor,uname,hname);

    	}
        return;
    }
    console.log("Full pull of " + hname);

    g_asker_pool[uname] = {is_pumper : false, is_muted : false, timestamp: 9999 /*fix*/};
	url = "https://www.quora.com/" + uname;
	var myFetch = fetch(url);
	myFetch.then(function(response) {
        response.text().then(function(text) {
          try {

          	text=skippast(text,"Answers<span class='list_count'>");
          	acount = scanto(text,"</span>");
          	acount=acount.replace(",","");
          	acount=parseInt(acount)
          	text=skippast(text,"Questions<span class='list_count'>");
          	qcount = scanto(text,"</span>");
          	qcount = qcount.replace(",","");
          	qcount=parseInt(qcount);
            g_asker_pool[uname].questions = qcount;
            g_asker_pool[uname].answers   = acount;

            
            if (pumpTest(uname)) {
              g_asker_pool[uname].is_pumper = true;
              displayPumpQuestion(ancestor,uname,hname);
              
            }

          	
          	console.log('!------- ' + qcount + ' ' + acount);

          	//console.log(text);
          } catch(err) {
            console.log(err);
          }
        });
    });
}



function getAsker(qhref)
{
   //console.log(qhref + "/log");
   var myFetch = fetch(qhref + "/log");

   myFetch.then(function(response) {
        response.text().then(function(text) {
          try {
            var url = response.url;
            
            url = scanto(url,'/log');
            ancestor = window.qlinks_seen[url];
            //console.log(text);
           
            text=skippast(text,'Question Log');
             
            text = skippast(text,'Question added by');
            if (text=='') {
               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by ??</span>';
            } else if (text.indexOf('Anonymous')>-1) {
               if (window.anon == 'highlight') {
                    ancestor.style.backgroundColor="#e7f4e6";
               } else {
                    ancestor.style.height=0;
                    ancestor.style.display="none";
               }

               asker = '<span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by Anonymous</span>';
               
            } else {
               text = skippast(text,"<a class='user' href='");
               text = scanto(text,"'");
               ulink = text;
               uname = skippast(text,"/profile/").replace(/-/g,' ');
               checkForQuestionPump(ulink,ancestor,uname);
               asker = '</a><span style="font-weight:500; font-size:12px; color : rgb(153,153,153);">&nbsp; -- asked by <a href="/profile/' + text + '">' + uname + '</a></span>';
            }
               
            qtext = ancestor.querySelector('.ui_qtext_rendered_qtext');
            qtext.outerHTML = qtext.outerHTML + asker;
            
          } catch (err) {
             console.log(err);
             //alert(err);
          }
      });
  });
}

function scanForAnonQ()
{

    var questions = document.getElementsByClassName("question_link");
    var i=0;
    for (i=0; i<questions.length; i++) {
       var ancestor = questions[i].closest('.multifeed_bundle_story');
       if (ancestor == null) continue;
       //alert(ancestor.innerHTML);

       var href = questions[i].href;
       
       if (!(href in window.qlinks_seen)) {
          //console.log(href);
          window.qlinks_seen[href] = ancestor;
          getAsker(href);
       }
    }
}

// https://stackoverflow.com/a/14570614

var observeDOM = (function(){
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
        eventListenerSupported = window.addEventListener;

    return function(obj, callback){
        if( MutationObserver ){
            // define a new observer
            var obs = new MutationObserver(function(mutations, observer){
                if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
                    callback();
            });
            // have the observer observe foo for changes in children
            obs.observe( obj, { childList:true, subtree:true });
        }
        else if( eventListenerSupported ){
            obj.addEventListener('DOMNodeInserted', callback, false);
            obj.addEventListener('DOMNodeRemoved', callback, false);
        }
    };
})();

/*
observeDOM( el,function(){ 
    console.log('XXXXXXXXXXXXXXXXXXXXXXXX ---   dom changed');
});
*/

window.addEventListener("beforeunload", function (event) {
   save_persistent();
});



(function() { 

   load_persistent();

   window.qlinks_seen = {};
   // This is my Stoyle!   Are you impressed by my Stoyle!   What is your Stoyle !?
   //newStyle = "<head><style> .side_bar{background-color:#c0c0c0 !important} .LoggedInSiteHeader{background-color:#aaaaaa !important;} body {background-color:#cccccc !important;} </style></head>";
   //document.body.insertAdjacentHTML( 'afterbegin', newStyle);



   var url = window.location.href;
   only_url = "quora.com/";
   if (url.slice(0-only_url.length) != only_url) {
     //alert("fail!" + url.slice(0-len(only_url)));
     //return;
   }


   var timer = null;


   newStyle = '';
   store = browser.storage.local.get();
   store.then((res) => { 
       var mode = res["mode"];
       var anon = res["anon"];

       try {
          browser.runtime.sendMessage({name: "banner_already_shown"}, function(response) {
            console.log(response.status);
            if ( !response.status && (mode != 'chill')) {
               showBanner();
            }
          });
        } catch(err) {
          ;
        }
       
       if (mode == 'chill') {
         newStyle='';
       } else if (mode == 'suppressed') {
         //showBanner();
       	 newStyle = '.HyperLinkFeedStory {display: none !important; height:0px !important;}';
       } else {
         //showBanner();
       	 newStyle = '.HyperLinkFeedStory {background-color: #ffffcc !important;}';
       }
   

       // Somebody ... brought a knife to a gun fight.
       newStyle += '.ui_layout_content_wrapper {all: initial; * {all: unset}}';
       newStyle += '.u-relative {all: initial; * {all: unset}}';
       newStyle += '.u-width--100 {all: initial; * {all: unset}} ';
       newStyle += '.u-margin-right--sm {all: initial; * {all: unset}} ';
       newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset}} ';
       newStyle += '.ui_qtext_truncate_4 {all: initial; * {all: unset}} ';
       newStyle += '.u-bg--cover {all: initial; * {all: unset;}}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {all: initial; * {all: unset}}';
       
       //newStyle += '.feed_card_on {all: initial; * {all: unset;}}';
       //newStyle += '.feed_item {background-color: initial; * {background-color: unset}}';
      
       newStyle += ".qure_mute_class .feed_item {background-color:#CCCCCC !important; padding-top: 0px !important; border:0 !important}";
       newStyle += ".qure_mute_class {max-height:500px; overflow: hidden; transition: duration 1s !important; transition-property: max-height !important;}";       

       //newStyle += '.ui_layout_thumbnail {all: initial; * {all: unset;}'};

       newStyle += '.header_image_with_site_header {display:none;}';
       newStyle += '.ui_layout_content_wrapper {width:100% !important;}';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {display:none !important;}';

       //newStyle += '.ui_qtext_rendered_qtext {width:572px !important}';

       //newStyle += '.ui_qtext_truncated_text {all: initial; * {all: unset;}} ';
       //newStyle += '.ui_qtext_rendered_qtext {font-size:15px; font-weight:400;}';    
       newStyle += '.ui_story_title ui_story_title_medium.ui_qtext_rendered_qtext {font-size: 18px; font-weight:700 !important}'; 

       newStyle += '.ui_qtext_truncated_text {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_qtext_truncated {font-size:15px !important; font-weight:400 !important; font-family: q_serif,Georgia,Times,"Times New Roman","Hiragino Kaku Gothic Pro","Meiryo",serif !important;}';
       newStyle += '.ui_header_image_wrapper {display:none !important};';
       newStyle += '.u-bg--cover {height:0px !important; width:0px !important; display:none !important;}';
       newStyle += '.u-width--100 {width:100% !important};';
       newStyle += '.ui_layout_thumbnail_wrapper--16-9 {width: 0px !important; display:none !important;}';
       newStyle += '.feed_card_on .Bundle .full_bundle {border: 1px solid #efefef !important; border-bottom: 0px;}';
       newStyle += '.sticky_bundle_wrapper {display: none;  xxdfn : true;}';
       newStyle = '<style>' + newStyle + '</style>';
    

       //document.body.insertAdjacentHTML( 'beforebegin', newStyle);
       document.head.insertAdjacentHTML('beforebegin',newStyle);
       document.head.insertAdjacentHTML( 'afterbegin', newStyle);
       document.head.insertAdjacentHTML( 'beforeend', newStyle);

       document.body.insertAdjacentHTML('afterbegin','<div class');
       console.log('---------------ZZZ');

       parent_img = document.getElementsByClassName("more_stories_offset");
       if (parent_img.length>0) {
          parent_img = parent_img[0];
          //parent_img.parent.removeChild(parent_img);
          console.log('ZZZZZZZZZZZZZZZZZZZZZZZ');
          observeDOM( parent_img,function() { 
            header_img = document.getElementsByClassName("ui_header_image_wrapper");
            if (header_img.length>0) {
               header_img = header_img[0];
               console.log("-- Before remove" + header_img.innerHTML);
               try {
                  header_img.parent.removeChild(header_img);
               } catch (err) {
                  console.log("Exception : " + err);
               } 
               console.log("-- After remove");
            }
            console.log('XXXXXXXXXXXXXXXXXXXXXXXX ---   dom changed');
            //head_img.parentNode.removeChild(head_img);
          });
          
       }

       
       // zoom
       if (anon != 'show') {
           window.anon = anon;
           scanForAnonQ();        
           //https://stackoverflow.com/questions/4620906/how-do-i-know-when-ive-stopped-scrolling-javascript
           window.addEventListener('scroll', function() {
                if(timer !== null) {
                    clearTimeout(timer);        
                }
                timer = setTimeout(function() {
                      scanForAnonQ();
                }, 150);
           }, false);
        }     
      
    });

})();