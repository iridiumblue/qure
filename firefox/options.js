function onError(error) {
  alert(error);
}

function save(e) {
   var val = "";
   var val2 = "";

   document.getElementById('link_color').backgroundColor;
   document.getElementById('anon_color').backgroundColor;

   if (document.getElementById('radio1').checked) {
      val = "highlight";
   } else if (document.getElementById('radio2').checked) {
      val = "suppressed";
   } else {
      val = "chill";
   }

   if (document.getElementById('radio21').checked) {
      val2 = "highlight";
   } else if (document.getElementById('radio22').checked) {
      val2 = "hide";
   } else {
      val2 = "show";
   }

  settingItem = browser.storage.local.set({
    mode: val,
    anon: val2,
    'link_color' : document.getElementById('link_color').value,
    'anon_color' : document.getElementById('anon_color').value
  });
  settingItem.then(null,onError);

  e.preventDefault();
}

function restoreOptions() {
  store = browser.storage.local.get();
  store.then((res) => { 
      var mode = res["mode"];
      var anon = res["anon"];
      var link_color = res["anon_color"];
      var anon_color = res["link_color"];

      if (typeof(mode)=='undefined') {mode='highlight';}
      if (typeof(anon)=='undefined') {anon='highlight';}
      if (typeof(link_color)=='undefined') {link_color='#ffffcc';}
      if (typeof(anon_color)=='undefined') {anon_color='#e7f4e6';}
      if (mode == 'highlight') {
            document.getElementById('radio1').checked = true;
      } else if (mode == 'suppressed') {
            document.getElementById('radio2').checked = true;
      } else {
            document.getElementById('radio3').checked = true;
      }

      if (anon == 'highlight') {
            document.getElementById('radio21').checked = true;
      } else if (anon == 'hide') {
            document.getElementById('radio22').checked = true;
      } else {
            document.getElementById('radio23').checked = true;
      }
      document.getElementById('link_color').value=link_color;
      document.getElementById('anon_color').value=anon_color;
  });

}

document.getElementById('save').addEventListener('click',save);
document.addEventListener('DOMContentLoaded', restoreOptions);